﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json.Linq;
using RestSharp;



namespace HomeAssignmentAxioma
{
    public partial class Form1 : Form
    {
        private ChromiumWebBrowser _browser;
        private delegate void InvokeDelegate();

        public Form1()
        {
            InitializeComponent();
            InitiateBrowser();
            InitializeChart();
        }

        private void InitiateBrowser()
        {
            var settings = new CefSettings();

            Cef.Initialize(settings);
            _browser = new ChromiumWebBrowser("https://www.google.com/maps");
            splitContainer1.Panel2.Controls.Add(_browser);
            splitContainer1.Dock = DockStyle.Fill;
            _browser.Dock = DockStyle.Fill;
        }

        private void InitializeChart()
        {
            this.chart1.Series["tempC"].MarkerStep = 1;
            this.chart1.Series["tempC"].BorderWidth = 3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.BeginInvoke(new InvokeDelegate(HandleLocationSelect));
        }
        
        private async void HandleLocationSelect()
        {
            var text = await _browser.GetSourceAsync();
            var regex = new Regex("(?<=(?:7.GLOBAL__gm2-headline-5\"> <span jstcache=\"[0-9]*\">|section-hero-header-title-subtitle\"> <span jstcache=\"[0-9]*\">))(.*?)(?=</span>)");
            var weatherRegex = new Regex("(?<=section-weather-text\"> <div jstcache=\"[0-9]*\">)(.*?)(?=</div> </div>)");
            var matches = regex.Matches(text);
            var weatherMatches = weatherRegex.Matches(text);
            if (matches.Count == 0 || weatherMatches.Count == 0) return;

            var weather = weatherMatches[0].ToString().Split('<').FirstOrDefault();
            var time = weatherMatches[0].ToString().Split('>').LastOrDefault();
            var result = "";

            foreach (var item in matches)
            {
                result += item + Environment.NewLine;
                var todayTemperatureValues = SendRequest(item.ToString());
                if (todayTemperatureValues.Count == 0) continue;

                SetGraphValues(todayTemperatureValues);
                break;
            }

            label1.Text = result + weather + Environment.NewLine + time;
        }

        private Dictionary<double, double> SendRequest(string cityName)
        {
            var todayTemperatureInCelsius = new Dictionary<double, double>();

            var baseUrl = new Uri(" http://api.worldweatheronline.com/premium/v1/past-weather.ashx");
            IRestClient client = new RestClient(baseUrl);
            IRestRequest request = new RestRequest("get", Method.GET);
            string currentDate = (DateTime.Now.AddDays(-1)).ToString("yyyy-MM-dd");

            request.AddParameter("q", cityName);
            request.AddParameter("date", currentDate);
            request.AddParameter("tp", "1");
            request.AddParameter("key", "a320ff8324414b7d87f214451201512");
            request.AddParameter("format", "json");
            IRestResponse response = client.Execute(request);

            if (Regex.IsMatch(response.Content, ".*error"))
            {
                this.label3.Text = "Pasirinktos vietovės orų prognozė nerasta";
                return todayTemperatureInCelsius;
            }
            
            dynamic res = JObject.Parse(response.Content);
            var weather = res.data.weather;

            foreach (var temp in weather[0].hourly)
            {
                todayTemperatureInCelsius.Add((double)temp.time / 100, (double)temp.tempC);
            }

            this.label3.Text = "Pasirinktos vietovės vakar dienos orų prognozė:";
            return todayTemperatureInCelsius;
        }

        private void SetGraphValues(Dictionary<double, double> todayTemperatureValues)
        {
            foreach (var todayTemperatureValue in todayTemperatureValues)
            {
                this.chart1.Series["tempC"].Points.AddXY(todayTemperatureValue.Key + 1, todayTemperatureValue.Value);
            }
        }

    }
}
